#! /bin/bash

set -eu

#sudo service wtvoice stop
echo "Copying files"
sudo cp assets/wtvoice.service /etc/systemd/system/wtvoice.service
sudo mkdir -p /usr/share/wtvoice
sudo cp -r src/ /usr/share/wtvoice
echo "Installing dependencies"
sudo cp composer.* /usr/share/wtvoice
cd /usr/share/wtvoice
sudo composer install
cd -
echo "Starting service"
sudo systemctl daemon-reload
sudo service wtvoice start
sudo systemctl enable wtvoice


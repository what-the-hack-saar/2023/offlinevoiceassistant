<?php
declare(strict_types=1);

namespace WTVoice;

use PhpMqtt\Client\MqttClient;
use WTVoice\Skills\DingDong;
use WTVoice\Skills\News;
use WTVoice\Skills\Time;
use WTVoice\Skills\Weather;

class ResponseGenerator {
    protected MqttClient $client;
    /** @var Array<string, Array<Skill>> $skills */
    protected array $skills;

    public function __construct(string $url, string $clientName) {
        //TODO replace by dynamic loading
        $allSkills = [new News(), new Time(), new Weather(), new DingDong()];
        foreach ($allSkills as $skillInstance) {
            foreach ($skillInstance->getTopics() as $topic) {
                $this->skills[$topic][] = $skillInstance;

            }
        }
        $this->client = new MqttClient($url, 12183, $clientName);
        $connected = false;
        while (!$connected) {
            try {
                $this->client->connect();
                $connected = true;
            } catch (Exception $e) {
                echo("No running mqtt broker detected on relevant host/port");
                sleep(10);
            }
        }
        echo ("Successfully connected, waiting for messages");
        $this->client->subscribe("hermes/intent/#", function($topic, $message, $retained, $matchedWildcards) {
            $this->generateResponse($topic, $message);
        });
        $this->client->subscribe("hermes/nlu/intentNotRecognized", function($topic, $message, $retained, $matchedWildcards) {
            $this->generateResponse($topic, $message);
        });
        $this->client->loop(true);

    }

    public function __destruct() {
        $this->client->disconnect();
    }

    public function generateResponse(string $topic, string $message): mixed {
        echo("received message on topic $topic, generating response.");
        if ($topic == "hermes/nlue/intentNotRecognized") {
            echo("Failure in intent recognisation");
        }
        else {
            $topicSuffix = $this->getTopicSuffix($topic);

            /** @var Array<Skill> $possibleSkills */
            $possibleSkills = [];
            foreach($this->skills as $topic => $skill) {
                if ($topicSuffix === $topic) {
                    $possibleSkills = $skill;
                }
            }
            if (empty($possibleSkills)) {
                return "Keine relevante Fertigkeit gefunden";
            }
            elseif (sizeof($possibleSkills) == 1) {
                echo "Fertigkeit gefunden, führe aus";
                $possibleSkills[0]->execute($this->client, $topic, $message);
                echo "Fertigkeit ausgeführt\n";
            }
            else {
                //TODO return query to select skill
            }
        }
    }

    private function getTopicSuffix(string $topic): string {
        $parts = explode("/", $topic);
        return $parts[sizeof($parts) - 1];
    }
}

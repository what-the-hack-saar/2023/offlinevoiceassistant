<?php
declare(strict_types=1);

namespace WTVoice;

use PhpMqtt\Client\MqttClient;

abstract class Skill {
    /** 
     * return a list of MQTT Topics (i.e. intents from Rhasspy) that the skill can reply to
     * example: return ['getNews'];
     * @return Array<string> */
    abstract function getTopics(): array;

    /**
     * A short description of the skill which can be read aloud for disambiguation purposes
     * if more than one skill can reply to a given topic
     */
    abstract function getDescription(): string;

    /**
     * Handle the actual skill execution
     * @return string Possible relevant voice output
     */
    protected abstract function activate(MqttClient $client, string $topic, array $message): string;

    function execute(MqttClient $client, string $topic, string $message): mixed {
        $messageDecoded = json_decode($message, true);
        $text = $this->activate($client, $topic, $messageDecoded);
        $siteId = $messageDecoded['siteId'];
        $output['text'] = $text;
        $output['siteId'] = $siteId;
        $jsonOutput = json_encode($output);
        $client->publish("hermes/tts/say", $jsonOutput);
    }

}

<?php

declare(strict_types=1);

namespace WTVoice\Skills;

use PhpMqtt\Client\MqttClient;
use WTVoice\Skill;

class DingDong extends Skill {
  function getTopics(): array
  {
    return ["GetTheDoor"];
  }

  function getDescription(): string
  {
    return "Spielt einen Klingelsound";
  }

  protected function activate(MqttClient $client, string $topic, array $message): string
  {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'http://localhost:12101/api/play-wav');
    // post wav file from asset folder via curl
    // Set wave content type header.
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: audio/wav']);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, [
      'file' => __DIR__ . '/../../../assets/dingdong.wav'
    ]);

    curl_exec($ch);
    curl_close($ch);

    return "Ha ha ha, wir hätten besser nicht geöffnet.";
  }
}
<?php

declare(strict_types=1);

namespace WTVoice\Skills;

use PhpMqtt\Client\MqttClient;
use WTVoice\Skill;

class News extends Skill {
  function getTopics(): array
  {
    return ['GetNews'];
  }

  function getDescription(): string
  {
    return "Die Nachrichten vorlesen";
  }

  protected function activate(MqttClient $client, string $topic, array $message): string
  {
    $rss = simplexml_load_file("https://www.tagesschau.de/index~rss2.xml");
    $items = $rss->channel->item;
    $description = $items[0]->description;
    $title = $items[0]->title;
    return $title . '. ' . $description;
  }
}
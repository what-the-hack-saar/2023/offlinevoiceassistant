<?php

declare(strict_types=1);

namespace WTVoice\Skills;

use DateTime;
use DateTimeZone;
use PhpMqtt\Client\MqttClient;
use WTVoice\Skill;

class Time extends Skill {
  function getTopics(): array
  {
    return ['GetTime'];
  }

  function getDescription(): string
  {
    return "Zeit und Datum vorlesen";
  }

  protected function activate(MqttClient $client, string $topic, array $message): string
  {
    setlocale(LC_TIME, "de_DE");
    $timeZone = "Europe/Berlin";
    $now = new DateTime("now", new DateTimeZone($timeZone));
    $hours = $now->format("H");
    $day = $now->format("j") . "ten";
    $month = $now->format("F");
    return "Keine Ahnung, irgendwas wie $hours Uhr am $day $month";
  }
}
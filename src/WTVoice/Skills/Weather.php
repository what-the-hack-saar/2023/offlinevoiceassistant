<?php

declare(strict_types=1);
namespace WTVoice\Skills;

use PhpMqtt\Client\MqttClient;
use WTVoice\Skill;

class Weather extends Skill {
    function getTopics() : array {
        return ['GetWeather'];
    }

    function getDescription() : string {
        return "Das Wetter vorlesen";
    }

    function activate(MqttClient $client, string $topic, array $message) : string {
        // create curl resource
        $ch = curl_init();
        $params = [];
        $params["current"] = "temperature_2m,apparent_temperature,windspeed_10m,precipitation,weathercode";
        $params["latitude"] = "49.2";
        $params["longitude"] = "7";
        $params["daily"] = "temperature_2m_max,temperature_2m_min,precipitation_probability_max";
        $params["timezone"] = "Europe%2FBerlin";
        $paramsString = '';
        foreach ($params as $name => $value) {
            $paramsString .= $name . '=' . $value . '&';
        }
        // set url
        curl_setopt($ch, CURLOPT_URL, "https://api.open-meteo.com/v1/forecast?" . $paramsString);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = json_decode(curl_exec($ch), true);
        print_r($output);
        // close curl resource to free up system resources
        curl_close($ch);
        
        $mintemp = round($output['daily']['temperature_2m_min'][0]);
        $maxtemp = round($output['daily']['temperature_2m_max'][0]);
        $currenttemp = round($output['current']['temperature_2m']);
        $apparenttemp = round($output['current']['temperature_2m']);
        $precipitationprob = $output['daily']['precipitation_probability_max'][0];
        
        return "Es sind heute zwischen $mintemp und $maxtemp Grad, derzeit $currenttemp Grad, gefühlt $apparenttemp Grad.
        Die Niederschlagswahrscheinlichkeit heute liegt bei $precipitationprob Prozent.";

    }
}
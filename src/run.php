<?php

include __DIR__ . DIRECTORY_SEPARATOR . "../vendor/autoload.php";

spl_autoload_register(function ($class) {
    $file = __DIR__ . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php';
    echo "$file\n";
    if (file_exists($file)) {
        require $file;
        return true;
    }
    return false;
});

use WTVoice\ResponseGenerator;

$generator = new ResponseGenerator('localhost', 'default');
